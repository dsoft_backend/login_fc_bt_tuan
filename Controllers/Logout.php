<?php

    echo "run logout...";
    session_start();

    include '../database/connect.php';
    include '../Models/User.php';

    session_destroy();
    header("Location: ../index.php?error=logout");
    exit; 
?>